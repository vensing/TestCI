#!/usr/bin/python3
# -*- coding: utf-8 -*-

import cv2
import numpy as np

rgb_72_62 = [206, 0, 1]
rgb_62_52 = [248, 123, 0]
rgb_52_42 = [197, 191, 3]
rgb_42_32 = [208, 200, 0]

rgbList = [rgb_72_62, rgb_62_52, rgb_52_42, rgb_42_32]


def TransformRGB2HSV(rgbList):
    hsvList = []
    for color in rgbList:
        h = np.array([[color[::-1]]], dtype='uint8')
        hsv = cv2.cvtColor(h, cv2.COLOR_BGR2HSV)
        hsvList.append([hsv[0, 0, 0], hsv[0, 0, 1], hsv[0, 0, 2]])
    return hsvList


hsvList = TransformRGB2HSV(rgbList)
print(hsvList)
